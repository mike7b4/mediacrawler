# Mediacrawler

For now scan for JPEG recursively in a directory and store it in a datastructure which then can be serialized as JSON.

This is based on my other homecloud/homeclud webserver projects.

# TODO ASAP

 -  [ ] Make it compile with kamadak-exif >= 0.5
 -  [ ] Document API's
 -  [ ] Release it on crates.io

# TODO later

  - [ ] Add support for other media.
  - [ ] Maybe add database support.
