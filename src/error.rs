use std::fmt;
#[derive(Debug)]
pub enum Error {
    IO(std::io::Error),
    Image(image::ImageError),
    CreateAlbum,
    AlbumAlreadyExists,
    AlbumNotFound,
}

impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Error {
        Error::IO(err)
    }
}

impl From<image::ImageError> for Error {
    fn from(err: image::ImageError) -> Error {
        Error::Image(err)
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match &*self {
            Error::IO(e) => write!(f, "{}", e),
            Error::Image(e) => write!(f, "Image {:?}.", e),
            Error::CreateAlbum => write!(f, "Create album failed."),
            Error::AlbumAlreadyExists => write!(f, "Album already exists."),
            Error::AlbumNotFound => write!(f, "Album not found."),
        }
    }
}
