use std::sync::mpsc::{channel, Sender};
use std::sync::{
    atomic::{AtomicBool, AtomicU8, Ordering},
    Arc, Mutex,
};
use std::thread::JoinHandle;
use std::time::{Duration, Instant};
pub trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>) {
        (*self)()
    }
}

type Job = Box<dyn FnBox + Send + 'static>;

pub struct ThreadPool {
    threads: Vec<JoinHandle<()>>,
    sender: Sender<Job>,
    pending: Arc<AtomicU8>,
}

impl Default for ThreadPool {
    fn default() -> Self {
        let (tx, rx) = channel();
        let rx = Arc::new(Mutex::new(rx));
        let mut pool = Self {
            threads: Vec::with_capacity(24),
            sender: tx,
            pending: Arc::new(AtomicU8::new(0)),
        };
        for _ in 0..24 {
            let rx = rx.clone();
            let pending = pool.pending.clone();
            let handle = std::thread::spawn(move || {
                while let Ok(job) = rx.lock().unwrap().recv() {
                    let now = Instant::now();
                    job.call_box();
                    pending.fetch_sub(1, Ordering::Relaxed);
                    log::info!("Job ended elapsed is: {:?}", now.elapsed());
                }
            });
            pool.threads.push(handle);
        }
        pool
    }
}

impl ThreadPool {
    pub fn push(&mut self, job: Job) {
        self.sender
            .send(job)
            .unwrap_or_else(|e| log::error!("Thread pool push failed cause: {}", e));
        self.pending.fetch_add(1, Ordering::Relaxed);
    }

    pub fn wait(&self, terminate: AtomicBool) {
        while !terminate.load(Ordering::Relaxed) && self.pending.load(Ordering::Relaxed) > 0 {
            std::thread::sleep(Duration::from_millis(50));
        }
    }
}
