use crate::error::Error;
use crate::image::Image;
use crate::threadpool::ThreadPool;
use serde::{Deserialize, Serialize};
use serde_json::json;
use sha2::{Digest, Sha256};
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::io::Write;
use std::path::PathBuf;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc::Sender;
use std::time::Duration;
#[derive(Serialize, Deserialize, Default, Clone)]
pub struct Albums {
    id: String,
    pub name: String,
    #[serde(skip_serializing)]
    pub directory: PathBuf,
    // files in current directory
    #[serde(skip_serializing)]
    images: Vec<Image>,
    pub albums: Vec<Albums>,
}

// Private
impl Albums {
    fn id(path: &PathBuf) -> String {
        let mut sha = Sha256::default();
        let mut id = String::with_capacity(64);
        sha.update(&path.to_str().unwrap().as_bytes());
        for byte in sha.finalize() {
            id += &format!("{:02x}", byte);
        }

        id
    }

    fn thumbnail(&self) -> Result<(), Error> {
        if self.images.is_empty() {
            return Ok(());
        }
        let mut num = rand::random::<usize>() % self.images.len();
        if num == self.images.len() {
            num -= 1;
        }
        let mut src = self.directory.clone();
        src.push(&self.images[num].file_name);
        let mut dest = dirs::data_dir().unwrap();
        dest.push(&format!("mediacrawler/{}.jpg", &self.id));
        if fs::metadata(&dest).is_ok() {
            return Ok(());
        }
        let thumb = image::open(src)?;
        let thumb = thumb.thumbnail(320, 200);
        let mut file = File::create(&dest)?;
        if let Err(err) = thumb.write_to(&mut file, image::ImageOutputFormat::Jpeg(50)) {
            log::error!("Create thumbnail {:?} {}", dest, err);
        }
        Ok(())
    }
}

impl Albums {
    pub fn new(path: PathBuf) -> Self {
        Self {
            id: Albums::id(&path),
            name: String::from(path.file_name().unwrap().to_str().unwrap_or("?")),
            directory: path,
            images: Vec::new(),
            albums: Vec::new(),
        }
    }

    pub fn create_album(&mut self, name: &str) -> Result<(), Error> {
        if name.find('/').is_some() {
            return Err(Error::CreateAlbum);
        }

        let mut path = self.directory.clone();
        path.push(name);
        if path.exists() {
            return Err(Error::AlbumAlreadyExists);
        }

        fs::create_dir(&path).map_err(|e| {
            eprintln!("{}", e);
            Error::CreateAlbum
        })?;

        self.insert(Albums::new(path));
        self.save();
        Ok(())
    }

    pub fn crawl(&mut self, tx: Sender<Albums>) -> Result<(), Error> {
        let mut pool = ThreadPool::default();
        for entry in fs::read_dir(&self.directory)? {
            let dir = entry?;
            if dir.metadata()?.is_dir() {
                let tx = tx.clone();
                pool.push(Box::new(move || {
                    let mut dir = Albums::new(dir.path());
                    match dir.crawl(tx.clone()) {
                        Ok(()) => {
                            tx.send(dir)
                                .map_err(|e| {
                                    log::error!("Send to thread failed: {}", e);
                                })
                                .unwrap();
                        }
                        Err(e) => {
                            log::error!(
                                "Crawl failed for: '{}' cause: {:?}",
                                dir.directory.display(),
                                e
                            );
                        }
                    };
                }));
            } else if let Some(ext) = dir.path().extension() {
                match &ext.to_str().unwrap_or("").to_lowercase()[0..] {
                    "jpg" | "jpeg" => {
                        let image = Image::new(dir.path());
                        self.images.push(image);
                    }
                    _ => {}
                }
            }
        }

        if let Err(e) = self.thumbnail() {
            log::error!(
                "Thumbnail failed on '{}' cause: {}",
                self.directory.display(),
                e
            );
        }
        self.images.sort_by(|a, b| a.file_name.cmp(&b.file_name));
        pool.wait(AtomicBool::new(false));
        Ok(())
    }

    pub fn recrawl(&mut self, id: &str, tx: Sender<Albums>) -> Result<(), Error> {
        for (index, album) in self.albums.iter().enumerate() {
            if album.id == id {
                self.albums
                    .remove(index)
                    .crawl(tx)
                    .unwrap_or_else(|e| log::warn!("Crawl failed {}", e));
                break;
            }
        }
        Err(Error::AlbumNotFound)
    }

    pub fn insert(&mut self, album: Albums) {
        for (i, ref a) in self.albums.iter().enumerate() {
            if album.name > a.name {
                self.albums.insert(i, album);
                return;
            }
        }

        self.albums.push(album);
    }

    pub fn images(&self, id: &str) -> Option<Vec<Image>> {
        if id.is_empty() {
            return None;
        }

        for album in &self.albums {
            if album.id == id {
                return Some(album.images.clone());
            }
        }

        None
    }

    // Lookup file
    pub fn thumbnail_path(&self, id: &str) -> Option<PathBuf> {
        if id == self.id {
            let mut path = dirs::data_dir().unwrap();
            path.push("homecloud");
            path.push(format!("{}.jpg", &id));
            return Some(path);
        }
        for album in &self.albums {
            if let Some(path) = album.thumbnail_path(id) {
                return Some(path);
            }
        }

        None
    }

    // Lookup file
    pub fn image_path(&self, id: &str) -> Option<PathBuf> {
        for image in &self.images {
            if image.id == id {
                let mut path = self.directory.clone();
                path.push(image.file_name.clone());
                return Some(path);
            }
        }

        // Search all albums to lookup file
        for album in &self.albums {
            if let Some(file) = album.image_path(&id) {
                return Some(file);
            }
        }

        None
    }

    pub fn save(&mut self) {
        if self.albums.is_empty() {
            return;
        }
        let mut file = dirs::config_dir().unwrap();
        file.push(&format!("mediacrawler/{}.json", &self.id));
        println!("Save: {:?} as {}.json", self.directory, self.id);
        match File::create(file) {
            Ok(mut file) => {
                let mut js = json!(self);
                /* This fields are normally skipped when serialize
                 * so we need to insert them. */
                js["directory"] = json!(self.directory);
                js["images"] = json!(self.images);
                for (i, ref album) in self.albums.iter().enumerate() {
                    js["albums"][i]["directory"] = json!(album.directory);
                    js["albums"][i]["images"] = json!(album.images);
                }
                if let Err(err) = file.write(&serde_json::to_string(&js).unwrap().as_bytes()) {
                    eprintln!("Bailed with: {} on '{:?}'", err, file);
                }
            }
            Err(e) => {
                println!("{} on '{}'", e, self.directory.to_str().unwrap_or(""));
            }
        }
    }

    pub fn load(path: PathBuf) -> Self {
        let id = Albums::id(&path);
        let mut conf = dirs::config_dir().unwrap();
        conf.push(&format!("mediacrawler/{}.json", &id));
        match File::open(conf) {
            Ok(file) => {
                print!("Load from: '{}.json' please wait...", &id);
                std::io::stdout().flush().unwrap();
                let reader = BufReader::new(file);
                let album: serde_json::Result<Albums> = serde_json::from_reader(reader);
                match album {
                    Ok(album) => {
                        println!(" OK");
                        album
                    }
                    Err(e) => {
                        eprintln!("{:?}", e);
                        Albums::new(path)
                    }
                }
            }
            Err(e) => {
                eprintln!("{:?} on '{}.json' rescan directories.", e, id);
                Albums::new(path)
            }
        }
    }
}

impl Drop for Albums {
    fn drop(&mut self) {
        //        self.save()
    }
}
