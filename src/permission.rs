#[derive(Clone)]
pub struct Permission {
    permission: Vec<String>,
}

impl Permission {
    pub fn new() -> Self {
        Self {
            permission: Vec::new(),
        }
    }

    fn allowed() -> bool {
        true
    }
}

impl Default for Permission {
    fn default() -> Self {
        Permission::new()
    }
}
