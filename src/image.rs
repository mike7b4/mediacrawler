use crate::permission::Permission;
use chrono::{DateTime, Utc};
use exif::{In, Tag};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use std::fmt;
use std::fs;
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use std::time::SystemTime;
#[derive(Serialize, Deserialize, Clone)]
pub struct Image {
    pub(crate) id: String,
    #[serde(skip)]
    permission: Permission,
    pub(crate) file_name: String,
    title: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    datetime: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    description: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    xdimension: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    ydimension: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    exposure_time: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    f_number: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    focal_length: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    flash: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    copyright: Option<String>,
}

impl Image {
    pub(crate) fn new(path: PathBuf) -> Self {
        let datetime: DateTime<Utc> = fs::metadata(&path)
            .unwrap()
            .created()
            .unwrap_or(SystemTime::UNIX_EPOCH)
            .into();
        let datetime: String = datetime.format("%c").to_string();
        let file_name = String::from(path.file_name().unwrap().to_str().unwrap_or(""));
        let title = String::from(path.file_stem().unwrap().to_str().unwrap_or(""))
            .replace("_", " ")
            .replace("-", " ");
        let mut sha = Sha256::default();
        let mut id = String::with_capacity(64);
        sha.update(&path.to_str().unwrap().as_bytes());
        for byte in sha.finalize() {
            id += &format!("{:02x}", byte);
        }

        let mut f = Self {
            id,
            permission: Permission::new(),
            file_name,
            title,
            description: None,
            datetime: Some(datetime),
            xdimension: None,
            ydimension: None,
            exposure_time: None,
            f_number: None,
            copyright: None,
            focal_length: None,
            flash: None,
        };

        if let Err(_) = f.load_exif(&path) {
            //error!("Could not read exif info for '{}'", &path.to_str().unwrap_or(""));
        }
        f
    }

    fn load_exif(&mut self, path: &PathBuf) -> Result<(), std::io::Error> {
        let file = File::open(path)?;
        let mut reader = BufReader::new(&file);
        let exif = exif::Reader::new()
            .read_from_container(&mut reader)
            .map_err(|e| {
                std::io::Error::new(
                    std::io::ErrorKind::Other,
                    format!("Exif load failed for '{}' cause: {}", path.display(), e),
                )
            })?;
        let mut tags = [
            (Tag::ImageDescription, &mut self.description),
            (Tag::DateTime, &mut self.datetime),
            (Tag::PixelXDimension, &mut self.xdimension),
            (Tag::PixelYDimension, &mut self.ydimension),
            (Tag::ExposureTime, &mut self.exposure_time),
            (Tag::FNumber, &mut self.f_number),
            (Tag::FocalLength, &mut self.focal_length),
            (Tag::Flash, &mut self.flash),
            (Tag::Copyright, &mut self.copyright),
        ];

        for (tag, ref mut field) in tags.iter_mut() {
            if let Some(value) = exif.get_field(*tag, In::PRIMARY) {
                **field = Some(value.display_value().to_string());
            }
        }

        Ok(())
    }
}

impl fmt::Display for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut s = format!("id: {}", self.id);
        s += &format!("file: {}\n", self.file_name);
        write!(f, "{}", s)
    }
}
